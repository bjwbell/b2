{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [0, 0, 800, 202],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutEmbeddedObject OBJECT",
          "rect": [0, 0, 402, 202],
          "reason": "style change"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

