{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutSVGRect rect",
          "rect": [10, 10, 460, 340],
          "reason": "style change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [10, 10, 460, 340],
          "reason": "bounds change"
        }
      ]
    }
  ]
}

