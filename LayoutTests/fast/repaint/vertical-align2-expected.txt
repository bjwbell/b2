{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [0, 0, 800, 246],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [0, 204, 800, 42],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow DIV id='target'",
          "rect": [200, 146, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV id='target'",
          "rect": [200, 100, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

