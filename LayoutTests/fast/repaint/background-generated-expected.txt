{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [8, 8, 100, 300],
          "reason": "border box change"
        },
        {
          "object": "LayoutBlockFlow DIV id='target'",
          "rect": [8, 208, 100, 100],
          "reason": "incremental"
        }
      ]
    }
  ]
}

