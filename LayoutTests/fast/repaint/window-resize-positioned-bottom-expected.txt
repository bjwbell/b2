{
  "bounds": [200, 300],
  "children": [
    {
      "bounds": [200, 300],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 200, 200, 100],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV",
          "rect": [0, 260, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV",
          "rect": [0, 160, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "reason": "forced by layout"
        }
      ]
    }
  ]
}
{
  "bounds": [300, 300],
  "children": [
    {
      "bounds": [300, 300],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 300, 300],
          "reason": "full"
        },
        {
          "object": "LayoutView #document",
          "rect": [200, 0, 100, 300],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "reason": "forced by layout"
        }
      ]
    }
  ]
}
{
  "bounds": [300, 250],
  "children": [
    {
      "bounds": [300, 250],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 250, 300, 50],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV",
          "rect": [0, 260, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV",
          "rect": [0, 210, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "reason": "forced by layout"
        }
      ]
    }
  ]
}
{
  "bounds": [250, 250],
  "children": [
    {
      "bounds": [250, 250],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 250, 250],
          "reason": "full"
        },
        {
          "object": "LayoutView #document",
          "rect": [250, 0, 50, 250],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "reason": "forced by layout"
        }
      ]
    }
  ]
}

