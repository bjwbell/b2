{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow (floating) DIV",
          "rect": [8, 8, 100, 100],
          "reason": "background obscuration change"
        },
        {
          "object": "LayoutBlockFlow DIV id='inner'",
          "rect": [8, 8, 100, 100],
          "reason": "bounds change"
        }
      ]
    }
  ]
}

