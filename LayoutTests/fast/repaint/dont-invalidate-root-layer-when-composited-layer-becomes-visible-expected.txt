{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow (positioned) DIV id='target'",
          "reason": "subtree"
        }
      ],
      "children": [
        {
          "position": [200, 200],
          "bounds": [200, 200]
        }
      ]
    }
  ]
}

