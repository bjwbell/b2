{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='outer'",
          "rect": [100, 60, 640, 240],
          "reason": "border box change"
        },
        {
          "object": "LayoutBlockFlow DIV id='inner2'",
          "rect": [100, 200, 600, 100],
          "reason": "layoutObject insertion"
        }
      ]
    }
  ]
}

