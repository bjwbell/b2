{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutText #text",
          "rect": [184, 201, 19, 160],
          "reason": "selection"
        },
        {
          "object": "InlineTextBox 'Some text in vertical mode'",
          "reason": "selection"
        },
        {
          "object": "LayoutBlockFlow (positioned) DIV id='test'",
          "reason": "selection"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "reason": "selection"
        }
      ]
    }
  ]
}

