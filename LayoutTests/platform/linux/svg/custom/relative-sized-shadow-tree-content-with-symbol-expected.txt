{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow div id='contentBox'",
          "rect": [8, 72, 402, 402],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutSVGContainer use",
          "rect": [9, 73, 400, 400],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRoot svg",
          "rect": [9, 73, 400, 400],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGViewportContainer svg id='targetSymbol'",
          "rect": [9, 73, 400, 400],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRect rect",
          "rect": [209, 273, 200, 200],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRect rect",
          "rect": [9, 73, 200, 200],
          "reason": "bounds change"
        },
        {
          "object": "LayoutSVGRect rect",
          "rect": [59, 273, 50, 50],
          "reason": "bounds change"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

