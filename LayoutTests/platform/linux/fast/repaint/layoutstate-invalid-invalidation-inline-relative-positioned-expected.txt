{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow (anonymous)",
          "rect": [8, 40, 784, 104],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutInline (relative positioned) SPAN id='target'",
          "rect": [8, 220, 100, 104],
          "reason": "bounds change"
        },
        {
          "object": "LayoutInline (relative positioned) SPAN id='target'",
          "rect": [8, 200, 100, 104],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [8, 220, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [8, 220, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV",
          "rect": [8, 200, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [8, 200, 100, 100],
          "reason": "bounds change"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "InlineFlowBox",
          "reason": "full"
        },
        {
          "object": "InlineFlowBox",
          "reason": "full"
        },
        {
          "object": "InlineFlowBox",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'x'",
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "reason": "bounds change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

