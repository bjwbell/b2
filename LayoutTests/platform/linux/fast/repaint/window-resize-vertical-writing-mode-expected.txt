{
  "bounds": [200, 300],
  "children": [
    {
      "bounds": [200, 300],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 200, 300],
          "reason": "full"
        },
        {
          "object": "LayoutView #document",
          "rect": [0, 200, 200, 100],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "rect": [144, 0, 56, 200],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [152, 8, 40, 184],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutText #text",
          "rect": [153, 8, 39, 140],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "rect": [164, 0, 36, 300],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [172, 8, 20, 284],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutText #text",
          "rect": [173, 8, 19, 236],
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'AAAA AAAA AAAA AAAA AAAA'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'AAAA AAAA AAAA'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'AAAA AAAA'",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}
{
  "bounds": [300, 300],
  "children": [
    {
      "bounds": [300, 300],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 300, 300],
          "reason": "full"
        },
        {
          "object": "LayoutView #document",
          "rect": [200, 0, 100, 300],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "rect": [264, 0, 36, 300],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "rect": [164, 0, 36, 300],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [272, 8, 20, 284],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [172, 8, 20, 284],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [273, 8, 19, 236],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [173, 8, 19, 236],
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'AAAA AAAA AAAA AAAA AAAA'",
          "reason": "bounds change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}
{
  "bounds": [300, 250],
  "children": [
    {
      "bounds": [300, 250],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 300, 250],
          "reason": "full"
        },
        {
          "object": "LayoutView #document",
          "rect": [0, 250, 300, 50],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "rect": [244, 0, 56, 250],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [252, 8, 40, 234],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutText #text",
          "rect": [253, 8, 39, 188],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "rect": [264, 0, 36, 300],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [272, 8, 20, 284],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutText #text",
          "rect": [273, 8, 19, 236],
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'AAAA AAAA AAAA AAAA AAAA'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'AAAA AAAA AAAA AAAA'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'AAAA'",
          "reason": "bounds change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}
{
  "bounds": [250, 250],
  "children": [
    {
      "bounds": [250, 250],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutView #document",
          "rect": [0, 0, 250, 250],
          "reason": "full"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "rect": [244, 0, 56, 250],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow HTML",
          "rect": [194, 0, 56, 250],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutView #document",
          "rect": [250, 0, 50, 250],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [252, 8, 40, 234],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [202, 8, 40, 234],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [253, 8, 39, 188],
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "rect": [203, 8, 39, 188],
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'AAAA AAAA AAAA AAAA'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'AAAA'",
          "reason": "bounds change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

