{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='target'",
          "rect": [102, 122, 50, 19],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutText #text",
          "rect": [102, 122, 39, 19],
          "reason": "full"
        },
        {
          "object": "InlineTextBox ''",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'after'",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

