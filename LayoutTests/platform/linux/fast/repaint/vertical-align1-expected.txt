{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [0, 0, 800, 104],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutBlockFlow BODY",
          "rect": [0, 100, 800, 5],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow DIV class='other'",
          "rect": [120, 80, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV class='other'",
          "rect": [120, 33, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV class='other'",
          "rect": [0, 80, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV class='other'",
          "rect": [0, 33, 20, 20],
          "reason": "bounds change"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "InlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

