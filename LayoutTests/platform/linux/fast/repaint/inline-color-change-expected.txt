{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutInline SPAN id='target'",
          "rect": [8, 72, 37, 19],
          "reason": "style change"
        },
        {
          "object": "LayoutText #text",
          "rect": [8, 72, 37, 19],
          "reason": "style change"
        },
        {
          "object": "InlineFlowBox",
          "reason": "style change"
        },
        {
          "object": "InlineTextBox 'PASS'",
          "reason": "style change"
        }
      ]
    }
  ]
}

