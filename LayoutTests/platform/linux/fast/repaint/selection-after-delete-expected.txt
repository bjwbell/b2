{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='test'",
          "rect": [38, 79, 152, 99],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutBlockFlow DIV id='test'",
          "rect": [38, 99, 152, 81],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow DIV id='test'",
          "rect": [38, 100, 152, 80],
          "reason": "incremental"
        },
        {
          "object": "LayoutText #text",
          "rect": [39, 79, 142, 99],
          "reason": "layoutObject removal"
        },
        {
          "object": "LayoutBlockFlow DIV id='test'",
          "rect": [38, 78, 3, 21],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox '\n'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox ' '",
          "reason": "full"
        },
        {
          "object": "InlineTextBox ' '",
          "reason": "full"
        },
        {
          "object": "InlineTextBox ' '",
          "reason": "full"
        },
        {
          "object": "InlineTextBox ' '",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'hello world hello world'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'hello world hello world'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'hello world hello world'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'hello world hello world'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'hello'",
          "reason": "full"
        },
        {
          "object": "LayoutBR BR",
          "reason": "bounds change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

