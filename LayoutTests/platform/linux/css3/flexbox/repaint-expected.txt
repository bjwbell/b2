{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='content'",
          "rect": [138, 128, 654, 100],
          "reason": "style change"
        },
        {
          "object": "LayoutBlockFlow P",
          "rect": [138, 128, 654, 100],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutText #text",
          "rect": [138, 128, 651, 99],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV id='content'",
          "rect": [148, 128, 644, 100],
          "reason": "style change"
        },
        {
          "object": "LayoutBlockFlow P",
          "rect": [148, 128, 644, 100],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutText #text",
          "rect": [148, 128, 640, 99],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV id='content'",
          "rect": [400, 128, 392, 160],
          "reason": "style change"
        },
        {
          "object": "LayoutBlockFlow P",
          "rect": [400, 128, 392, 160],
          "reason": "forced by layout"
        },
        {
          "object": "LayoutBlockFlow DIV id='left'",
          "rect": [8, 228, 392, 60],
          "reason": "incremental"
        },
        {
          "object": "LayoutText #text",
          "rect": [400, 128, 389, 159],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV id='left'",
          "rect": [148, 128, 252, 160],
          "reason": "incremental"
        },
        {
          "object": "LayoutBlockFlow DIV id='left'",
          "rect": [138, 128, 10, 100],
          "reason": "incremental"
        },
        {
          "object": "InlineTextBox 'Aliquam ut turpis nisl, in vulputate sapien. Cum sociis natoque'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet dolor id urna eleifend aliquet. Nulla'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet dolor id urna eleifend aliquet. Nulla'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean laoreet dolor id urna eleifend aliquet. Nulla'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'Pellentesque erat lectus, ultricies a lobortis id, faucibus id quam.'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'Pellentesque erat lectus, ultricies a lobortis id, faucibus id quam.'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'Sed congue magna vitae dolor feugiat vehicula. Sed volutpat,'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'condimentum leo neque sed nulla. Nunc quis porta elit.'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'elit. Pellentesque erat lectus, ultricies a lobortis id, faucibus id quam.'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'elit. Pellentesque erat lectus, ultricies a lobortis id, faucibus id quam.'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'laoreet dolor id urna eleifend aliquet. Nulla vel dolor ipsum.'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'parturient montes, nascetur ridiculus mus. Sed congue magna vitae dolor feugiat vehicula. Sed volutpat, tellus'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'parturient montes, nascetur ridiculus mus. Sed congue magna vitae dolor feugiat vehicula. Sed volutpat,'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'parturient montes, nascetur ridiculus mus. Sed congue magna vitae dolor feugiat vehicula. Sed volutpat,'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'penatibus et magnis dis parturient montes, nascetur ridiculus mus.'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'tellus vel varius vestibulum, purus quam mollis sapien, in condimentum leo neque sed nulla. Nunc quis porta'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'tellus vel varius vestibulum, purus quam mollis sapien, in condimentum leo neque sed nulla. Nunc quis porta'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'tellus vel varius vestibulum, purus quam mollis sapien, in'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'vel dolor ipsum. Aliquam ut turpis nisl, in vulputate sapien. Cum sociis natoque penatibus et magnis dis'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'vel dolor ipsum. Aliquam ut turpis nisl, in vulputate sapien. Cum sociis natoque penatibus et magnis dis'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'vel dolor ipsum. Aliquam ut turpis nisl, in vulputate sapien. Cum sociis natoque penatibus et magnis dis'",
          "reason": "bounds change"
        },
        {
          "object": "InlineTextBox 'vel varius vestibulum, purus quam mollis sapien, in condimentum leo neque sed nulla. Nunc quis porta elit.'",
          "reason": "bounds change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

