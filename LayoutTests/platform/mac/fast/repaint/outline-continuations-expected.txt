{
  "bounds": [800, 600],
  "children": [
    {
      "bounds": [800, 600],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow (anonymous)",
          "rect": [16, 146, 202, 22],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutBlockFlow (anonymous)",
          "rect": [16, 108, 202, 22],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutInline SPAN id='outer'",
          "rect": [16, 108, 88, 60],
          "reason": "style change"
        },
        {
          "object": "LayoutInline SPAN id='outer'",
          "rect": [16, 146, 88, 22],
          "reason": "style change"
        },
        {
          "object": "InlineFlowBox",
          "reason": "style change"
        },
        {
          "object": "InlineFlowBox",
          "reason": "style change"
        },
        {
          "object": "InlineTextBox 'CONTENTS'",
          "reason": "full"
        },
        {
          "object": "InlineTextBox 'CONTENTS'",
          "reason": "full"
        },
        {
          "object": "LayoutBlockFlow (anonymous)",
          "reason": "forced by layout"
        },
        {
          "object": "LayoutInline SPAN id='outer'",
          "reason": "style change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ]
    }
  ]
}

