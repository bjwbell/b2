{
  "bounds": [1418, 1008],
  "children": [
    {
      "bounds": [1418, 1008],
      "contentsOpaque": true,
      "drawsContent": true,
      "paintInvalidations": [
        {
          "object": "LayoutBlockFlow DIV id='container'",
          "rect": [8, 8, 769, 108],
          "reason": "invalidate paint rectangle"
        },
        {
          "object": "LayoutBlockFlow (anonymous)",
          "rect": [8, 116, 769, 54],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow (anonymous)",
          "rect": [8, 62, 769, 54],
          "reason": "bounds change"
        },
        {
          "object": "LayoutBlockFlow DIV id='container'",
          "rect": [8, 62, 769, 54],
          "reason": "incremental"
        },
        {
          "object": "LayoutText #text",
          "reason": "bounds change"
        },
        {
          "object": "LayoutText #text",
          "reason": "bounds change"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        },
        {
          "object": "RootInlineBox",
          "reason": "full"
        }
      ],
      "children": [
        {
          "shouldFlattenTransform": false,
          "children": [
            {
              "position": [8, 8],
              "bounds": [1000, 1000]
            },
            {
              "position": [8, 8],
              "bounds": [1000, 104],
              "drawsContent": true,
              "paintInvalidations": [
                {
                  "object": "LayoutBlockFlow (relative positioned) DIV class='mv-tile'",
                  "rect": [0, 108, 1000, 50],
                  "reason": "subtree"
                },
                {
                  "object": "LayoutBlockFlow (relative positioned) DIV class='mv-tile'",
                  "rect": [0, 54, 1000, 50],
                  "reason": "compositing update"
                },
                {
                  "object": "LayoutBlockFlow (relative positioned) DIV class='mv-tile'",
                  "rect": [0, 54, 100, 50],
                  "reason": "subtree"
                },
                {
                  "object": "LayoutBlockFlow (relative positioned) DIV class='mv-tile'",
                  "rect": [0, 0, 100, 50],
                  "reason": "compositing update"
                },
                {
                  "object": "LayoutBlockFlow (relative positioned) DIV id='foo' class='mv-tile'",
                  "rect": [0, 0, 100, 50],
                  "reason": "layoutObject removal"
                },
                {
                  "object": "InlineBox",
                  "reason": "full"
                },
                {
                  "object": "InlineBox",
                  "reason": "full"
                }
              ]
            }
          ]
        }
      ]
    }
  ]
}

